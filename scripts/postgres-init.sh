#!/usr/bin/env bash

set -euo pipefail

THIS_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "${THIS_SCRIPT_DIR}/.." || exit

python << END
from sys import argv

from covid_19.database import init_db

if __name__ == '__main__':
    try:
        init_db(argv[1])
    except IndexError:
        init_db('production')
END

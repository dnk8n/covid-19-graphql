#!/usr/bin/env bash

set -euo pipefail

THIS_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "${THIS_SCRIPT_DIR}/.." || exit

# TODO: If more services are added to docker-compose, this command has to be changed
docker-compose down "$@"

#!/usr/bin/env bash

set -euo pipefail

THIS_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "${THIS_SCRIPT_DIR}/.." || exit

function postgres_ready(){
python << END
import sys
from sqlalchemy.exc import OperationalError
from covid_19 import database
try:
    database.engine.connect()
except OperationalError:
    sys.exit(-1)
else:
    sys.exit(0)
END
}

cp -n .env.tpl covid_19/.env

docker-compose up -d "$@" postgres

until postgres_ready; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - continuing..."

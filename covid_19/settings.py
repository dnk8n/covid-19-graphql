import os
from pathlib import Path
import logging

logging.basicConfig()

BASE_DIR = Path(__file__).parent.resolve()
DATABASE_URL = os.getenv('DATABASE_URL')

if not DATABASE_URL:
    from dotenv import load_dotenv
    load_dotenv(dotenv_path=(BASE_DIR / '.env').resolve())
    DATABASE_URL = os.getenv('DATABASE_URL')
    assert DATABASE_URL

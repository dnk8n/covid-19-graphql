from datetime import datetime, timedelta
import io
import json
import urllib.request

import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

from covid_19.settings import BASE_DIR, DATABASE_URL
from covid_19.models import Base, Country, EventDate, Event, FuzzyCountryLookup

POPULATION_CSV_FILE = BASE_DIR / 'data/population.csv'

engine = create_engine(DATABASE_URL, client_encoding='utf8')
Base.metadata.create_all(engine)
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))


def regenerate_population_csv():
    with urllib.request.urlopen('https://population.un.org/wpp/Download/Files/1_Indicators%20(Standard)/CSV_FILES/'
                                'WPP2019_TotalPopulationBySex.csv') as population_url:
        population_df = pd.read_csv(io.StringIO(population_url.read().decode('UTF-8')))
    population_df = population_df[population_df['Time'] == 2020]
    population_df = population_df.drop(['LocID', 'VarID', 'Variant', 'Time', 'MidPeriod'], axis='columns')
    population_df = population_df.drop_duplicates()
    population_df = population_df.set_index('Location')
    population_df.to_csv(POPULATION_CSV_FILE)


def init_db(grade):
    # Database is generated from source files
    with urllib.request.urlopen('https://pomber.github.io/covid19/timeseries.json') as covid19_timeseries_url:
        covid19_timeseries_data = json.load(covid19_timeseries_url)

    population_df = pd.read_csv(POPULATION_CSV_FILE).set_index('Location')

    start_date_string = '01/22/2020'  # Date data starts
    start_date = datetime.strptime(start_date_string, '%m/%d/%Y').date()
    end_date = datetime.utcnow().date() + timedelta(days=1)  # One day from now to ensure we are covered

    for idx, n in enumerate(range((end_date - start_date).days + 1)):
        db_session.add(EventDate(id=idx, day=start_date + timedelta(days=n)))

    for country_id, (country, events) in enumerate(covid19_timeseries_data.items()):
        try:
            country_stats = population_df.loc[country]
        except KeyError:
            alt_names = {
                'Bolivia': 'Bolivia (Plurinational State of)',
                'Brunei': 'Brunei Darussalam',
                'Congo (Brazzaville)': 'Congo',
                'Congo (Kinshasa)': 'Congo',
                'Cote d\'Ivoire': 'Côte d\'Ivoire',
                'Diamond Princess': None,
                'Iran': 'Iran (Islamic Republic of)',
                'Korea, South': 'Republic of Korea',
                'Moldova': 'Republic of Moldova',
                'MS Zaandam': None,
                'Russia': 'Russian Federation',
                'Taiwan*': 'China, Taiwan Province of China',
                'Tanzania': 'United Republic of Tanzania',
                'US': 'United States of America',
                'Venezuela': 'Venezuela (Bolivarian Republic of)',
                'Vietnam': 'Viet Nam',
                'Syria': 'Syrian Arab Republic',
                'Laos': 'Lao People\'s Democratic Republic',
                'West Bank and Gaza': None,
                'Kosovo': None,
                'Burma': 'Myanmar',
            }
            try:
                alt_name = alt_names[country]
            except KeyError:
                from fuzzywuzzy import process
                # TODO: Check generated database to see if there are Fuzzy matches that need to be explicitly handled
                choices = population_df.index.values.tolist()
                alt_name, score = process.extractOne(country, choices)
                if not grade == 'production':
                    db_session.add(FuzzyCountryLookup(country_id=country_id, alt_name=alt_name, score=score))
            if alt_name:
                country_stats = population_df.loc[alt_name]
        try:
            population_male = int(round(country_stats['PopMale'] * 1000))
            population_female = int(round(country_stats['PopFemale'] * 1000))
            population_total = int(round(country_stats['PopTotal'] * 1000))
        except ValueError:
            population_male = None
            population_female = None
            population_total = None
        db_session.add(
            Country(
                id=country_id,
                name=country,
                population_male=population_male,
                population_female=population_female,
                population_total=population_total,
                population_density=country_stats['PopDensity']
            )
        )
        for event_id, event in enumerate(events):
            # TODO: Assert that event id corresponds to date id
            # date_day = datetime.strptime(event['date'], '%Y-%m-%d').date()
            # assert event_id == date_day
            confirmed = event['confirmed']
            deaths = event['deaths']
            recovered = event['recovered']
            db_session.add(
                Event(country_id=country_id, event_date_id=event_id, confirmed=confirmed, death=deaths, recovered=recovered)
            )
    if grade == 'production':
        FuzzyCountryLookup.__table__.drop(engine)
    db_session.commit()


if __name__ == '__main__':
    init_db('production')

from sqlalchemy import Column, Date, ForeignKey, Integer, Float, String, PrimaryKeyConstraint
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class EventDate(Base):
    __tablename__ = 'event_date'
    id = Column(Integer, primary_key=True)
    day = Column(Date, unique=True)


class Country(Base):
    __tablename__ = 'country'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    population_male = Column(Integer)
    population_female = Column(Integer)
    population_total = Column(Integer)
    population_density = Column(Float)


class Event(Base):
    __tablename__ = 'event'
    country_id = Column(Integer, ForeignKey(Country.id))
    event_date_id = Column(Integer, ForeignKey(EventDate.id))
    confirmed = Column(Integer)
    death = Column(Integer)
    recovered = Column(Integer)
    __table_args__ = (PrimaryKeyConstraint(country_id, event_date_id),)


class FuzzyCountryLookup(Base):
    __tablename__ = 'fuzzy_country_lookup'
    id = Column(Integer, primary_key=True)
    country_id = Column(Integer, ForeignKey(Country.id))
    alt_name = Column(String)
    score = Column(Integer)
